<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<nav>
		<div class="nav-wrapper grey lighten-1">
			<div class="col l6 s12">
				<a href="index.php" class="breadcrumb black-text atitraukimasHome">Home</a>
				<a href="#!" class="breadcrumb black-text">News</a>
			</div>
		</div>
	</nav>
	<body>
		<br>
		<div class="row">
			<div class="margin col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="tamsiojiera.php" class="col s12 m12 l6"><img class="singleNaujas1" src="images/devilstone.jpg"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Devil Stone</b></h5></div>
						<div class="col s12 m12 l6"><p class="pirmaEilute">The "Devil Stone" Festival collapsed into a large crowd of metal fans in 2009. The festival gathered around 1900. On arrival, the festival can be arranged in specially prepared tent camps. There are volleyball competitions in the festival, it is possible to swim in the Sventoji River. After the festival there is the worst photo competition, in which the participants of the festival send their photos from the festival. The most important criteria for the content of the festival are relevance, creativity, professionalism. In Devilston, live music is performed on five stages (Gama Scene, East Scene, Western Scene, Turbo Scene, Barbablu Scene). There are 35-50 foreign and Lithuanian groups annually. During the festival, participants can engage in musical mastery with lessons, workshops, networking, football, movie shows, visit artist exhibitions.</p>
						<div class="col s12 m12 l6">
							<a href="tamsiojiera.php" class="btn waves-effect grey darken-1 text-black" type="submit" name="action pozicija">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="margin col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="granatos.php" class="col s12 m12 l6"><img class="singleNaujas1" src="images/granatos.jpg"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Granatos</b></h5></div>
						<div class="col s12 m12 l6"><p class="pirmaEilute">Overnight. If you have chosen accommodation in ToiCamp or Green Granatos Live tent campus, the organizers of the festival recommend that you choose the entrance to the Lithuanian Folk Museum, and if you plan to stay in the blue or yellow tent camp, choose the main entrance of the festival. This saves you time and reduces the distance to the required location. By the way, tents can only be built in tent camps, and access to them will be controlled. The tent procedure will be monitored by volunteers - please follow their instructions and save space. Those who intend to stay afloat at the festival unplannedly and will not arrange hotel bills will be able to settle in a free campground.</p>
						<div class="col s12 m12 l6">
							<a href="granatos.php" class="btn waves-effect grey darken-1 text-black" type="submit" name="action">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="galapagai.php" class="col s12 m12 l6"><img class="singleNaujas1" src="images/galapagai.jpg"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Galapagai</b></h5></div>
						<div class="naujas col s12 m12 l6">
						<p class="pirmaEilute">"Galapagai" is an electronic music and active entertainment festival taking place in the island of Zarasas on the summer. Several dozens of Lithuanian and foreign music performers from Germany, Finland, Sweden, Belarus, Latvia, Poland, Estonia and other countries appear on five festivals annually. During the event sports competitions and extreme shows are held, friendly dogs and their hosts race are organized. The festival supports organizations that assist animal welfare. "Galapagai" is the only Lithuanian festival participating in the European Talent Exchange Program (ETEP) and has been nominated for several successive years in the category of the best small European festivals.</p>
						<div class="naujas col s12 m12 l6">
							<a href="galapagai.php" class="btn waves-effect grey darken-1 text-black" type="submit" name="action">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
			<div class="row">
			<div class="margin col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="coachella.php" class="col s12 m12 l6"><img class="singleNaujas1 zemyn" src="images/coachella.png"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Coachella</b></h5></div>
						<div class="col s12 m12 l6"><p class="pirmaEilute">The Coachella Valley Music and Arts Festival (commonly referred to as Coachella or the Coachella Festival) is an annual music and arts festival held at the Empire Polo Club in Indio, California, located in the Inland Empire's Coachella Valley in the Colorado Desert. It was co-founded by Paul Tollett and Rick Van Santen in 1999, and is organized by Goldenvoice, a subsidiary of AEG Live.[1] The event features musical artists from many genres of music, including rock, indie, hip hop, and electronic dance music, as well as art installations and sculptures. Across the grounds, several stages continuously host live music. The main stages are the: Coachella Stage, Outdoor Theatre, Gobi Tent, Mojave Tent, and Sahara Tent; a smaller Oasis Dome was used in 2006 and 2011, while a new Yuma stage was introduced in 2013 and a Sonora stage in 2017.</p>
						<div class="col s12 m12 l6">
							<a href="coachella.php" class="btn waves-effect grey darken-1 text-black bntKeisti" type="submit" name="action pozicija">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="margin col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="tomorrowland.php" class="col s12 m12 l6"><img class="singleNaujas1" src="images/tomorrowland.png"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Tomorrowland</b></h5></div>
						<div class="col s12 m12 l6"><p class="pirmaEilute">2011 marked the festival's expansion to three days occurring on 22, 23, 24 July. Tomorrowland 2011 had the theme "The Tree of Life" and the anthem "The Way We See The World" by Dimitri Vegas & Like Mike featuring Afrojack and Nervo.[4] Only a few days after the official pre-sale of tickets kicked off Tomorrowland was completely sold out and had over 180,000 visitors. David Guetta, Nervo, Swedish House Mafia, Avicii, Tiësto, Hardwell, Carl Cox, Paul van Dyk, Tensnake, Laidback Luke, Brodinski, Juanma Tudon, Mike Matthews, De Jeugd van Tegenwoordig, and dozens of others performed. It was voted the Best Music Event by the International Dance Music Awards for the first time in 2012.</p>
						<div class="col s12 m12 l6">
							<a href="tomorrowland.php" class="btn waves-effect grey darken-1 text-black" type="submit" name="action">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s10 offset-s1 white z-depth-3 height">
				<div class="row">
					<div class="col s12">
						<a href="creamfields.php" class="col s12 m12 l6"><img class="singleNaujas1" src="images/creamfields.jpg"></a>
						<div class="col s12 m12 l6 text-align center"><h5><b>Creamfields</b></h5></div>
						<div class="naujas col s12 m12 l6">
						<p class="pirmaEilute">Creamfields is a major dance music festival series founded and organised by British club promoter Cream, with its flagship UK edition taking place on August Bank Holiday weekend, with a number of international editions held across various territories worldwide. First held in 1998 in Winchester, the festival moved to Cream's home city of Liverpool the following year, taking place on the old Liverpool airport, before moving to its current location on the Daresbury estate in Cheshire. Having initially begun as a one-day event with 25,000 people in attendance, the festival is now a four-day event with camping options, which attracts 70,000 attendees each day. Creamfields is now one of the largest electronic music festivals in the world in terms of attendance, number and size of stages, and size and depth of lineup, regularly featuring headline performances from a wide range of mainstream and underground DJs and performers.</p>
						<div class="naujas col s12 m12 l6">
							<a href="creamfields.php" class="btn waves-effect grey darken-1 text-black" type="submit" name="action">More
								<i class="material-icons right">send</i>
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>




		<?php include "footer.php"; ?>
	</body>
	</html>