<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<nav>
		<div class="nav-wrapper grey lighten-1">
			<div class="col l6 s12">
				<a href="index.php" class="breadcrumb black-text atitraukimasHome">Home</a>
				<a href="#!" class="breadcrumb black-text">About us</a>
				<a href="#!" class="breadcrumb black-text">Rokas</a>
			</div>
		</div>
	</nav>

	<body>
		<div class="row">
			<div class="col s12">
				<h2>Rokas</h2>
				<div class="col s12 white z-depth-3 height">
					<div class="col s12 m12 l6"><img class="singleNaujas1" src="images/cat.jpg"></div>
					<div class="col s12 m12 l6 pirmaEilute"><p>Esu... Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12 m6 l4">
				<div class="row">
					<div class=" col s12 m10 offset-m1 l10 offset-l7">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/dunno.jpg" height="220">
							</div> 
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Trumpai apie Evaldą<i class="material-icons right">more_vert</i></span>
								<p><a href="Evaldas.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Trumpai apie Evaldą<i class="material-icons right">close</i></span>
								<p class="intro">Do Jewish vampires still avoid crosses?</p>
								<p class="intro">If an ambulance is on its way to save someone, and it runs someone over, does it stop to help them?</p>
								<p class="intro">When Atheists go to court, do they have to swear on the bible?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col m6 l4 s12 ">
				<div class="row">
					<div class="col s12 m10 offset-m1 l10 offset-l7">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/logo.png" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Trumpai apie Aistę<i class="material-icons right">more_vert</i></span>
								<p><a href="aiste.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Trumpai apie Aistę<i class="material-icons right">close</i></span>
								<p>VCS</p>
							</div>
						</div>
					</div>
				</div>					
			</div>			
		</div>

		<?php include "footer.php"; ?>
		
	</body>
	</html>