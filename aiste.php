<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<nav>
		<div class="nav-wrapper grey lighten-1">
			<div class="col l6 s12">
				<a href="index.php" class="breadcrumb black-text atitraukimasHome">Home</a>
				<a href="#!" class="breadcrumb black-text">About us</a>
				<a href="#!" class="breadcrumb black-text">Aistė</a>
			</div>
		</div>
	</nav>

	<body>
		<div class="row">
			<div class="col s12">
				<h2>Aistė</h2>
				<div class="col s12 white z-depth-3 height">
					<div class="col s12 m12 l6"><img class="singleNaujas1" src="images/logo.png"></div>
					<div class="col s12 m12 l6 pirmaEilute"><p align="center"><b>Mano Festivalių top 5</b>
						<ul align="center">
							<li><a href="https://global.lollapalooza.com" target="_blank">Lollapalooza</a></li>
							<li><a href="https://lifeisbeautiful.com" target="_blank">Life is Beautiful</a></li>
							<li><a href="https://www.coachella.com/home/" target="_blank">Coachella</a></li>
							<li><a href="https://www.bonnaroo.com" target="_blank">Bonnaroo</a></li>
							<li><a href="https://www.voodoofestival.com" target="_blank">Voodoo</a></li>
						</ul></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col s12 m6 l4">
					<div class="row">
						<div class=" col s12 m10 offset-m1 l10 offset-l7">
							<div class="card">
								<div class="card-image waves-effect waves-block waves-light">
									<img class="activator" src="images/dunno.jpg" height="220">
								</div> 
								<div class="card-content">
									<span class="card-title activator grey-text text-darken-4">Trumpai apie Evaldą<i class="material-icons right">more_vert</i></span>
									<p><a href="Evaldas.php">Plačiau</a></p>
								</div>
								<div class="card-reveal">
									<span class="card-title grey-text text-darken-4">Trumpai apie Evaldą<i class="material-icons right">close</i></span>
									<p class="intro">Do Jewish vampires still avoid crosses?</p>
									<p class="intro">If an ambulance is on its way to save someone, and it runs someone over, does it stop to help them?</p>
									<p class="intro">When Atheists go to court, do they have to swear on the bible?</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col m6 l4 s12 ">
					<div class="row">
						<div class="col s12 m10 offset-m1 l10 offset-l7">
							<div class="card">
								<div class="card-image waves-effect waves-block waves-light">
									<img class="activator" src="images/logo.png" height="220">
								</div>
								<div class="card-content">
									<span class="card-title activator grey-text text-darken-4">Trumpai apie Roką<i class="material-icons right">more_vert</i></span>
									<p><a href="Rokas.php">Plačiau</a></p>
								</div>
								<div class="card-reveal">
									<span class="card-title grey-text text-darken-4">Trumpai apie Roką<i class="material-icons right">close</i></span>
									<p>Informacija</p>
								</div>
							</div>
						</div>
					</div>					
				</div>			
			</div>

			<?php include "footer.php"; ?>
		</body>
		</html>