$(document).ready(function(){
	new Chartist.Line('.ct-chart5', {
  labels: [1, 2, 3, 4, 5, 6, 7, 8],
  series: [
    [1, 2, 3, 1, -2, 0, 1, 0],
    [-2, -1, -2, -1, -2.5, -1, -2, -1],
    [0, 0, 0, 1, 2, 2.5, 2, 1],
    [2.5, 2, 1, 0.5, 1, 0.5, -1, -2.5]
  ]
}, {
  high: 3,
  low: -3,
  showArea: true,
  showLine: false,
  showPoint: false,
  fullWidth: true,
  axisX: {
    showLabel: false,
    showGrid: false
  }
});
    var data = {
  labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri','Sat','Sun'],
  series: [
    [4, 6, 8, 6, 4, 7, 3]
  ],
    fullWidth: true,
};
new Chartist.Line('.ct-chart', data);
  });
  $(document).ready(function(){
var data = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    series: [
    [8, 7, 6, 7, 6, 5, 6, 6, 7, 8, 9, 10],
  ]
};
var options = {
  seriesBarDistance: 15
};
var responsiveOptions = [
  ['screen and (min-width: 641px) and (max-width: 1024px)', {
    seriesBarDistance: 10,
    axisX: {
      labelInterpolationFnc: function (value) {
        return value;
      }
    }
  }],
  ['screen and (max-width: 640px)', {
    seriesBarDistance: 5,
    axisX: {
      labelInterpolationFnc: function (value) {
        return value[0];
      }
    }
  }]
];
new Chartist.Bar('.ct-chart2', data, options, responsiveOptions);
});
$(document).ready(function(){
var count = 45;
var max = 100;
var chart = new Chartist.Bar('.ct-chart3', {
  labels: Chartist.times(count),
  series: [
    Chartist.times(count).map(Math.random).map(Chartist.mapMultiply(max))
  ]
}, {
  axisX: {
    showLabel: false
  },
  axisY: {
    onlyInteger: true
  }
});
chart.on('draw', function(context) {
  if(context.type === 'bar') {
    context.element.attr({
      style: 'stroke: hsl(' + Math.floor(Chartist.getMultiValue(context.value) / max * 100) + ', 50%, 50%);'
    });
  }
});
});
$(document).ready(function(){
   var data = {
  labels: ['Priklauso nuo...', 'Guminukai', 'Saldumynai'],
  series: [20, 30, 50]
};
var options = {
  labelInterpolationFnc: function(value) {
    return value[0]
  }
};
var responsiveOptions = [
  ['screen and (min-width: 640px)', {
    chartPadding: 30,
    labelOffset: 100,
    labelDirection: 'explode',
    labelInterpolationFnc: function(value) {
      return value;
    }
  }],
  ['screen and (min-width: 1024px)', {
    labelOffset: 80,
    chartPadding: 20
  }]
];
new Chartist.Pie('.ct-chart4', data, options, responsiveOptions);
  });
	  
	  
	  
	  