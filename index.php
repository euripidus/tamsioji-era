<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<nav>
		<div class="nav-wrapper grey lighten-1">
			<div class="col l6 s12">
				<a href="#!" class="breadcrumb black-text atitraukimasHome">Home</a>
			</div>
		</div>
	</nav>
	<body>
		<div class="still">
			<div class="carousel-slider">
				<div class="mySlides fade karuseles1">
				</div>
				<div class="mySlides fade karuseles2">
				</div>
				<div class="mySlides fade karuseles3">
				</div>
				<div class="mySlides fade karuseles4">
				</div>
				<div class="mySlides fade karuseles5">
				</div>
				<div class="mySlides fade karuseles6">
				</div>
				<div class="mySlides fade karuseles7">
				</div>
				<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides(1)">&#10095;</a>
				<div class="stiliusKar">
				<audio controls="controls">
					<source src="12.mp3" type="audio/mpeg" />
				</audio>
			</div>
			</div>
			<br>
			<div style="text-align:center">
				<span class="dot" onclick="currentSlide(1)"></span> 
				<span class="dot" onclick="currentSlide(2)"></span> 
				<span class="dot" onclick="currentSlide(3)"></span> 
				<span class="dot" onclick="currentSlide(4)"></span> 
				<span class="dot" onclick="currentSlide(5)"></span> 
				<span class="dot" onclick="currentSlide(6)"></span>
				<span class="dot" onclick="currentSlide(7)"></span>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="row">
					<div class="col s10 offset-s1">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/og.jpg" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Tomorrowland<i class="material-icons right">more_vert</i></span>
								<p><a target="_blank" href="tomorrowland.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Tomorrowland<i class="material-icons right">close</i></span>
								<p class="pirmaEilute">Tomorrowland is an electronic dance music festival held in Boom, Belgium. Tomorrowland was first held in 2005 and has since become one of the world's largest and most notable music festivals. It now stretches over 2 weekends and usually sells out in minutes.</p>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="row">
					<div class="col s10 offset-s1">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/1478.jpg" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Coachella!<i class="material-icons right">more_vert</i></span>
								<p><a target="_blank" href="coachella.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Coachella!<i class="material-icons right">close</i></span>
								<p class="pirmaEilute">Koačelos slėnio muzikos ir menų festivalis, arba Koačelos festivalis, Koačela (angl. Coachella Valley Music and Arts Festival, Coachella Festival, Coachella) – kasmetinis trijų dienų muzikos bei menų festivalis, kurį įkūrė Paul Tollett, organizuoja Goldenvoice, o jis vyksta Indijaus mieste, Kalifornijoje, Inland Empire Koačelos slėnyje. Renginyje galima išgirsti daugybę muzikos žanrų: rokas, indie rock, hip hop bei elektroninė muzika, ir pamatyti didžiules meno skulptūras. Festivalis turi kelias scenas-palapines, kurios būna pastatytos per visą slėnio plotą, kiekvienoje jų visą renginio laiką be perstojo groja gyva muzika. Pagrindinės scenos: Koačelos scena, Vasaros teatras, Gobio palapinė, Mojave palapinė ir Saharos palapinė.</p>
							</div>
						</div>
					</div>					
				</div>	
			</div>			
			<div class="col s12 m6 l4">
				<div class="row">
					<div class="col s10 offset-s1">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/2589.jpg" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Creamfields<i class="material-icons right">more_vert</i></span>
								<p><a target="_blank" href="creamfields.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Creamfields<i class="material-icons right">close</i></span>
								<p class="pirmaEilute">
									Creamfields is a major dance music festival series founded and organised by British club promoter Cream, with its flagship UK edition taking place on August Bank Holiday weekend, with a number of international editions held across various territories worldwide.

									First held in 1998 in Winchester, the festival moved to Cream's home city of Liverpool the following year, taking place on the old Liverpool airport, before moving to its current location on the Daresbury estate in Cheshire.

								Having initially begun as a one-day event with 25,000 people in attendance, the festival is now a four-day event with camping options, which attracts 70,000 attendees each day.</p>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
		<?php include "footer.php"; ?>
	</body>
	</html>