<!DOCTYPE html>
<html>
<head>
  <title>Festival List</title>

  <?php include "header.php"; ?>

<div class="container">
    <div class="carousel">
    <a class="carousel-item" href="#one!"><img src="images/galapagai2.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="images/galapagai3.jpg"></a>
    <a class="carousel-item" href="#three!"><img src="images/galapagai4.jpg"></a>
    <a class="carousel-item" href="#four!"><img src="images/galapagai5.jpg"></a>
    <a class="carousel-item" href="#five!"><img src="images/galapagai6.jpg"></a>
    <a class="carousel-item" href="#one!"><img src="images/granatos2.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="images/granatos3.jpg"></a>
    <a class="carousel-item" href="#three!"><img src="images/granatos4.jpg"></a>
    <a class="carousel-item" href="#four!"><img src="images/granatos5.jpg"></a>
    <a class="carousel-item" href="#five!"><img src="images/granatos6.jpg"></a>
  </div>

<ul class="collection">
    <li class="collection-item avatar">
      <img src="images/galapagai.jpg" alt="" class="circle">
      <span class="title"><a href="galapagai.php">Galapagai</a></span>
      <p>27th-28th of July 2019<br>
         Zarasas Island
      </p>
      <a href="https://kakava.lt/renginys/muzikos-festivalis-granatos-live/1020/1102/?theme=true&eid=1020" class="secondary-content">Buy Ticket</a>
    </li>
    <li class="collection-item avatar">
      <img src="images/granatos.jpg" alt="" class="circle">
      <span class="title"><a href="granatos.php">Granatos</a></span>
      <p>2nd-4th of August 2019<br>
         Rumsiskes
      </p>
      <a href="http://galapagai.lt/bilietai/" class="secondary-content">Buy Ticket</a>
    </li>
    <li class="collection-item avatar">
      <img src="images/devilstone.jpg" alt="" class="circle">
      <span class="title"><a href="TamsiojiEra.php">Devil Stone</a></span>
      <p>11th-14th of July 2019<br>
         Anyksciai
      </p>
      <a href="http://www.devilstone.net/bilietai/va-bilietai/" class="secondary-content">Buy Ticket</a>
    </li>
  </ul>
</div>
</div>
    
    <?php include "footer.php"; ?>
</body>
</html>