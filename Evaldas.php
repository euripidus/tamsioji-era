<!DOCTYPE html>
<html>
<head>
	<?php include "header.php"; ?>
	<nav>
		<div class="nav-wrapper grey lighten-1">
			<div class="col l6 s12">
				<a href="index.php" class="breadcrumb black-text atitraukimasHome">Home</a>
				<a href="#!" class="breadcrumb black-text">About us</a>
				<a href="#!" class="breadcrumb black-text">Evaldas</a>
			</div>
		</div>
	</nav>

	<body>
		<div class="row">
			<div class="col s12">
				<h2>Evaldas</h2>
				<div class="col s12 white z-depth-3 height">
					<div class="col s12 m12 l6"><img class="singleNaujas1" src="images/11.jpg"></div>
					<div class="col s12 m12 l6 pirmaEilute"><p>Esu Evaldas. Bzzzz. ką tik prazvimbė visai nemaža medūza. Visa tokia gauruota it senas Beno šo, tik žinoma, be odegos, o tai būtų visai linksma, jeigu ji dar būtų buvusi ir juoda. Esu kartografas, na netikras toks šiek tiek, dirbtinis, tačiau pats procesas visai smagus, kadangi studijos tik dar labiau paskatina pačią kūrybą. Norėčiau dabar tiesiog pasakyti miau, juk taip linksmiau. Argi ne? Pritariat ir spaudžiat šypsenelė, ot velniokai... :D</p>
						<p>O čia jums mįslė... Koks daiktas pasaulyje yra ilgiausias ir trumpiausias, greičiausias ir lėčiausias, smulkiausiai dalijasi ir plačiausiai tęsiasi, mažiausiai paisomas ir labiausiai apgailimas, be ko niekas negali įvykti, kas naikina visa, kas menka, ir gaivina visa, kas didu?</p>
						<p>Na o žemiau pateikiu mūsų tris didvyrius, kurie sugebėjo su mumis susitvarkyti per tą trumpą laiką!</p>
						<img class="singleNaujas1" src="images/1aa.jpg"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="card col s12 m12 l12 offset-l6">
					<h5 class="text-align center">Ko suvalgau daugiausiai</h5>
					<div class="ct-chart4 ct-series-a ct-slice-pie ct-perfect-fourth ct-golden-section"></div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="card col s12 m12 l12 offset-l6">
					<h5 class="text-align center">Oro sąlygų poveikis</h5>
					<div class="ct-chart5 ct-series-a ct-slice-pie ct-perfect-fourth ct-golden-section"></div>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="card col s12 m12 l12">
					<h5 class="text-align center">Darbingumas per savaitę (10 balų sistemoje)</h5>
					<div class="ct-chart ct-perfect-fourth ct-golden-section"></div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="card col s12 m12 l12">
					<h5 class="text-align center">Kiek praleidžiu laiko lovoje (valandomis, skirtingais mėnesiais)</h5>
					<div class="ct-chart2 ct-perfect-fourth ct-golden-section"></div>
				</div>	
			</div>
			<div class="col s12 m6 l4">
				<div class="card col s12 m12 offset-m6 l12">
					<h5 class="text-align center">Nuotaikų kaita, priklausomai nuo kitų savo asmenybių (per dieną)</h5>
					<div class="ct-chart3 ct-perfect-fourth ct-golden-section"></div>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="row">
					<div class=" col s12 m10 offset-m1 l10 offset-l7">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/ghost.jpg" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Trumpai apie Roką<i class="material-icons right">more_vert</i></span>
								<p><a href="Rokas.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Trumpai apie Roką<i class="material-icons right">close</i></span>
								<p class="naujas">VCS</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col m6 l4 s12 ">
				<div class="row">
					<div class="col s12 m10 offset-m1 l10 offset-l7">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light">
								<img class="activator" src="images/logo.png" height="220">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4">Trumpai apie Aistę<i class="material-icons right">more_vert</i></span>
								<p><a href="aiste.php">Plačiau</a></p>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4">Trumpai apie Aistę<i class="material-icons right">close</i></span>
								<p>VCS</p>
							</div>
						</div>
					</div>
				</div>					
			</div>			
		</div>

		<?php include "footer.php"; ?>
	</body>
	</html>